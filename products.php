<?php
class Product{

    // Connection instance and table name
    private $connection;
    private $table_name = "products";

    // table columns
    public $product_id;
    public $name;
    public $images;
    public $available_sizes;
    public $price;
    public $category;
    public $created_at;
    
    // constructor with $connection as database connection
    public function __construct($connection){
        $this->connection = $connection;
    }
    // Read Products
    public function read(){
        $query = "SELECT * FROM " . $this->table_name . "";
        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        return $stmt;
    }
    // Read Single Product 
    function readOne(){        
        $query = "SELECT * FROM " . $this->table_name . " WHERE product_id = ? ";
        $stmt = $this->connection->prepare( $query );
        $stmt->bindParam(1, $this->id);
        $stmt->execute();
        
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $this->product_id = $row['product_id'];
        $this->name = $row['name'];
        $this->images = $row['images'];
        $this->available_sizes = $row['available_sizes'];
        $this->price = $row['price'];
        $this->category = $row['category'];
        $this->created_at = $row['created_at'];
        $this->updated_at = $row['updated_at'];
    }
    // Create Product
    public function create(){
        
        $query = "INSERT INTO " . $this->table_name . " SET name=:name, images=:images, available_sizes=:available_sizes, price=:price, category=:category, created_at=:created_at";
        
        $stmt = $this->connection->prepare($query);     
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->images=htmlspecialchars(strip_tags($this->images));
        $this->available_sizes=htmlspecialchars(strip_tags($this->available_sizes));
        $this->price=htmlspecialchars(strip_tags($this->price));
        $this->category=htmlspecialchars(strip_tags($this->category));
        $this->created_at=htmlspecialchars(strip_tags($this->created_at));

        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":images", $this->images);
        $stmt->bindParam(":available_sizes", $this->available_sizes);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":category", $this->category);
        $stmt->bindParam(":created_at", $this->created_at);

        if($stmt->execute()){
            return true;
        }

        return false;
    }
    // Update Product
    function update(){
        
        $query = "UPDATE " . $this->table_name . " SET name=:name, images=:images, available_sizes=:available_sizes, price=:price, category=:category WHERE product_id = :product_id";
        
        $stmt = $this->connection->prepare($query);
        
        // sanitize
        $this->product_id=htmlspecialchars(strip_tags($this->product_id));
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->images=htmlspecialchars(strip_tags($this->images));
        $this->available_sizes=htmlspecialchars(strip_tags($this->available_sizes));
        $this->price=htmlspecialchars(strip_tags($this->price));
        $this->category=htmlspecialchars(strip_tags($this->category));

        $stmt->bindParam(':product_id', $this->product_id);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":images", $this->images);
        $stmt->bindParam(":available_sizes", $this->available_sizes);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":category", $this->category);

        if($stmt->execute()){
            return true;
        }

        return false;
    }
    // Delete Product
    public function delete(){

        $query = "DELETE FROM " . $this->table_name . " WHERE product_id = ?";
        
        $stmt = $this->connection->prepare($query);
        
        // sanitize
        $this->product_id=htmlspecialchars(strip_tags($this->product_id));

        $stmt->bindParam(1, $this->product_id);

        if($stmt->execute()){
            return true;
        }
        
        return false;
    }
    // search products
    function search($keywords){
        $query = "SELECT * FROM " . $this->table_name . " WHERE name LIKE ? OR category LIKE ? ORDER BY created_at DESC";
        $stmt = $this->connection->prepare($query);
        // sanitize
        $keywords=htmlspecialchars(strip_tags($keywords));
        $keywords = "%{$keywords}%";

        $stmt->bindParam(1, $keywords);
        $stmt->bindParam(2, $keywords);

        $stmt->execute();

        return $stmt;
    }
}