<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$seconds_to_cache = 3600;
$ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
header("Expires: $ts");
header("Pragma: cache");
header("Cache-Control: max-age=$seconds_to_cache");

function require_auth() {
  $AUTH_USER = 'admin';
  $AUTH_PASS = 'admin';
  $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
  $is_not_authenticated = (
    !$has_supplied_credentials ||
    $_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
    $_SERVER['PHP_AUTH_PW']   != $AUTH_PASS
  );
  if ($is_not_authenticated) {
    header('HTTP/1.1 401 Authorization Required');
    header('WWW-Authenticate: Basic realm="Access denied"');
    echo "Access Denied";
    exit;
  }
}

require_auth();

// included database and object files
include_once $_SERVER['DOCUMENT_ROOT'].'/products/config/dbclass.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/products/products.php';

// created database and product object
$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$product = new Product($connection);

$data->id = isset($_GET['id']) ? $_GET['id'] : die();

$product->product_id = $data->id;

if($product->delete()){ 

    // set response code - 200 ok
    http_response_code(200);

    echo json_encode(array("message" => "Product was deleted."));
}
else{
	
    // set response code - 503 service unavailable
    http_response_code(503);
    
    echo json_encode(array("message" => "Unable to delete product."));
}
?>