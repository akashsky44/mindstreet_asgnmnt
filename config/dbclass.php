<?php
class DBClass {

    private $host = "localhost";
    private $username = "akash";
    private $password = "clinic";
    private $database = "street";

    public $connection;

    // Database connection
    public function getConnection(){

        $this->connection = null;

        try{
            
            $this->connection = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->database, $this->username, $this->password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        }catch(PDOException $exception){
            echo "Error: " . $exception->getMessage();
        }

        return $this->connection;
    }
}
?>