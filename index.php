<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$seconds_to_cache = 3600;
$ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
header("Expires: $ts");
header("Pragma: cache");
header("Cache-Control: max-age=$seconds_to_cache");

function require_auth() {
  $AUTH_USER = 'admin';
  $AUTH_PASS = 'admin';
  $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
  $is_not_authenticated = (
    !$has_supplied_credentials ||
    $_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
    $_SERVER['PHP_AUTH_PW']   != $AUTH_PASS
  );
  if ($is_not_authenticated) {
    header('HTTP/1.1 401 Authorization Required');
    header('WWW-Authenticate: Basic realm="Access denied"');
    echo "Access Denied";
    exit;
  }
}

require_auth();

// included database and object files
include_once $_SERVER['DOCUMENT_ROOT'].'/products/config/dbclass.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/products/products.php';

// created database and product object
$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$product = new Product($connection);

$stmt = $product->read();
$count = $stmt->rowCount();

if($count > 0){

    $products = array();
    $products["body"] = array();
    $products["count"] = $count;
    
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $product_item  = array(
              "product_id" => $product_id,
              "name" => $name,
              "images" => $images,
              "available_sizes" => $available_sizes,
              "price" => $price,
              "category" => $category,
              "created_at" => $created_at,
              "updated_at" => $updated_at
        );

        array_push($products["body"], $product_item);
    }
    // set response code - 200 OK
    http_response_code(200);

    echo json_encode($products);
}
else {
    // set response code - 404 Not found
    http_response_code(404);
    
    echo json_encode(
        array("message" => "No products found.")
    );
}
?>